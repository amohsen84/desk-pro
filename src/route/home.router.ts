import { flatten } from './../utils/help.methods';
import { Router } from "express";
import { Logger } from "../lib/logger";


const logger = Logger.getInstance();
const router = Router() as Router;

// Define all routes and their handlers
router.post("/", (req, res) => {
    try {
        // Read the payload
        const payload = req.body.payload ;
        logger.debug("Payload: ", payload);
        // Flatten the array
        const flattenedArray = flatten(payload);
        logger.debug("FlattenedArray: ", flattenedArray);
        // Return the array sorted alphabatically 
        res.json({response: flattenedArray.sort()});
     }
     catch(e) {
         logger.error("Error occured", e);
         res.status(500).json({ message: "Unexpected error occurred."});
     }
});

export default router;