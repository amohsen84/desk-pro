import * as awsServerlessExpress from "aws-serverless-express";
import { Application } from "./lib/application";

/**
 * This file is used by lambda functions in AWS to direct API requests to express application 
 */
// Express APP
const app = Application.getInstance();
// Create a server running express 
const server = awsServerlessExpress.createServer(app, null, []);
// Passes all requests to the server  
module.exports.processRequests = (event, context) => awsServerlessExpress.proxy(server, event, context);
