export const flatten = (a: Array<any>) => {
    return a.reduce((flat, i) => {
        if (Array.isArray(i)) {
        return flat.concat(flatten(i));
        }
        return flat.concat(i);
    }, []);
};

export const sortArray = (value: string) => {

};
