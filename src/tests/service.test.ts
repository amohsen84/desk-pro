import "mocha";
import { testApplication as app } from "../testrunner";
import { successfulRequest, successfulResponse, unsuccessfulRequest} from "./test.data";
import * as chai from "chai";

describe("desk-pro requests", () => {

    function testPost(request: string, expectedHttpResponseCode: number = 200, postData: any, contentType: RegExp = /json/) {
        return app.post(request)
            .set("Content-Type", "application/json")
            .send(postData)
            .expect(expectedHttpResponseCode)
            .expect("content-type", contentType);
    }

    describe("Successful data", () => {
        it("POST Should return a successful response", done => {
            testPost("/desk-pro", 200, successfulRequest)
                .end((err, res) => {
                    if(err) {
                        done(err);
                    }
                    else {
                        console.log("resonse", res.body);
                        console.log("E-resonse", successfulResponse);
                        chai.expect(res.body).to.deep.equal(successfulResponse);
                        done();
                    }
                });
        });
    });


    describe("Unsuccessful data", () => {
        it("POST Should return unexpected error.", done => {
            testPost("/desk-pro", 500, unsuccessfulRequest)
                .end((err, res) => err ? done(err) : done());
        });
    });
});
