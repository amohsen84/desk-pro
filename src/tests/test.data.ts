export const successfulRequest ={
	"payload": [
		["my", [[["abc", "zzz"]]], "aa"]
	]
};

export const successfulResponse = {
	"response": [
		"aa", "abc", "my", "zzz"
	]
};

// individual field replacement
export const unsuccessfulRequest = {
};
