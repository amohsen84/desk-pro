Desk-Pro-Coding Challenge

Overview
=======================
A function  to flatten then sort an array of arrays:
Input: ["gamma", ["alpha", ["beta"]], [[["delta"]]]]
Output: ["alpha", "beta", "delta", "gamma"]


Installation
---------------
 run the following command to install dependencies:

    > cd {Project-Directory}
    > npm install


Transpilation
---------------------
The code is written in type script. Run the following commands to transpile it:

    > cd {Project Directory}
    > npm run build


Project Structure
-----------------
    /src
      /interfaces    # Interfaces definitions.
      /lib           # classes that facilitate the implementation.
      /middleware    # Middleware
      /route         # Routes definitions (end points)
      /tests         # Test cases definitions
    /config          # Define configurations such as constants.


Testing
---------------------
To run the unit test and generate the coverage document (the document will be generated at {Project Directory}/document), you need to run the following commands:

    > cd {Project Directory}
    > npm test

The coverage document will be generated at {Project Directory}/coverage. It includes details about how the unit tests covered the code showing lines covered and not covered by the unit tests.

Running it locally
---------------------
You can run the API locally using the following command:

    > cd {Project Directory}
    > npm start


API Documentation
---------------------
The API documentation is dynamically generated using swagger. You need to update the swagger.json file when you modify/add an api method. The API documentation is hosted in AWS at:

    https://7c1d1mhdp7.execute-api.eu-west-1.amazonaws.com/dev/api-docs/

It is an interactive documentation that you can use it to test the API.


Deployment to AWS
---------------------
Serverless is used to deploy the application to AWS.
To configure serverless with your AWS account, please follow the instructions at:               

    https://serverless.com/framework/docs/providers/aws/guide/credentials/

To deploy the code to AWS (lambda functions), you need to run the following commands:

    > cd {Project Directory}
    > npm run deploy

The API is currently hosted in AWS and the API URL is as follow:

    https://7c1d1mhdp7.execute-api.eu-west-1.amazonaws.com/dev/desk-pro


Code Documentation
---------------------
We are using TYPE DOC to generate code documentation, The comments need to follow the guidelines specified at               

    http://typedoc.org/guides/doccomments/.

You can generate/update the  code documentation by running the following command:

    > cd {Project Directory}
    > npm run build-doc


